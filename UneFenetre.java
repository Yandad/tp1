import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.concurrent.Semaphore;

import javax.swing.*;

class UneFenetre extends JFrame 
{
    public UnMobile sonMobile;
    public int sonNbMobiles;
    public boolean[] bouton;
    public JButton[] arret;
    public Thread[] tache;
    public UnMobile lesMobiles[];
    public Thread lesTaches[];
    private final int LARG=400, HAUT=250; 
    
    public UneFenetre(int telNbMobiles, int telNbRessources)
    {
	// TODO 
	// ajouter sonMobile a la fenetre
    	super("Le Mobile");
    	Container Conteneur = getContentPane();
    	sonNbMobiles = telNbMobiles;
    	Conteneur.setLayout(new GridLayout(sonNbMobiles, 1));
    	lesMobiles = new UnMobile[sonNbMobiles];
    	lesTaches = new Thread[sonNbMobiles];
 
    	/*JPanel panel = new JPanel();
    	panel.setLayout(new GridLayout(10 ,2));
    	this.add(panel);*/
    	
    	for (int i = 0 ; i < sonNbMobiles ; i ++){
    		
    		//int y = i/100;

    		lesMobiles[i] = new UnMobile(LARG, HAUT / sonNbMobiles);
    		Conteneur.add(lesMobiles[i]);
    		lesTaches[i] = new Thread(lesMobiles[i]);
    		
    		/*sonMobile = new UnMobile(380, i/10);
    		panel.add(sonMobile);
    		bouton[y] = true;
    		arret[y] = new JButton("Marche/Arret");
    		arret[y].addActionListener((ActionListener) this);
    		panel.add(arret[y]);
    		tache[y] = new Thread(sonMobile);
    		tache[y].start();*/
    	}
           //Il s'agit d'une association entre la Fenetre et le mobile mais c'est naviguable uniquement de Fenetre vers Mobile
    	
    	for (int i = 0 ; i < sonNbMobiles ; i ++){
    		lesTaches[i].start();
    	}
    	
	    
 
	
	
    	this.setSize(LARG, HAUT);
    	this.setVisible(true);
    }
    
  
    	
    }

